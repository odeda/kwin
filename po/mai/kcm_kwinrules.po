# translation of kcmkwinrules.po to Maithili
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sangeeta Kumari <sangeeta09@gmail.com>, 2009.
# Rajesh Ranjan <rajesh672@gmail.com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kcmkwinrules\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-07 02:19+0000\n"
"PO-Revision-Date: 2010-01-29 13:59+0530\n"
"Last-Translator: Rajesh Ranjan <rajesh672@gmail.com>\n"
"Language-Team: Maithili <maithili.sf.net>\n"
"Language: mai\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"
"\n"

#: kcmrules.cpp:226
#, kde-format
msgid "Copy of %1"
msgstr ""

#: kcmrules.cpp:406
#, kde-format
msgid "Application settings for %1"
msgstr "%1 क' लेल अनुप्रयोग बिन्यास"

#: kcmrules.cpp:428 rulesmodel.cpp:215
#, kde-format
msgid "Window settings for %1"
msgstr "%1 क' लेल विंडो बिन्यास"

#: optionsmodel.cpp:198
#, kde-format
msgid "Unimportant"
msgstr "महत्वहीन"

#: optionsmodel.cpp:199
#, kde-format
msgid "Exact Match"
msgstr "सटीक जोडीदार"

#: optionsmodel.cpp:200
#, kde-format
msgid "Substring Match"
msgstr "सबस्ट्रिंग जोडीदार"

#: optionsmodel.cpp:201
#, kde-format
msgid "Regular Expression"
msgstr "नियमित (रेगुलर) एक्सप्रेशन."

#: optionsmodel.cpp:205
#, kde-format
msgid "Apply Initially"
msgstr "आरंभ मे लागू करू"

#: optionsmodel.cpp:206
#, kde-format
msgid ""
"The window property will be only set to the given value after the window is "
"created.\n"
"No further changes will be affected."
msgstr ""

#: optionsmodel.cpp:209
#, kde-format
msgid "Apply Now"
msgstr "अखन लागू करू"

#: optionsmodel.cpp:210
#, kde-format
msgid ""
"The window property will be set to the given value immediately and will not "
"be affected later\n"
"(this action will be deleted afterwards)."
msgstr ""

#: optionsmodel.cpp:213
#, kde-format
msgid "Remember"
msgstr "याद राखू"

#: optionsmodel.cpp:214
#, kde-format
msgid ""
"The value of the window property will be remembered and, every time the "
"window is created, the last remembered value will be applied."
msgstr ""

#: optionsmodel.cpp:217
#, kde-format
msgid "Do Not Affect"
msgstr "प्रभावित नहि करू"

#: optionsmodel.cpp:218
#, kde-format
msgid ""
"The window property will not be affected and therefore the default handling "
"for it will be used.\n"
"Specifying this will block more generic window settings from taking effect."
msgstr ""

#: optionsmodel.cpp:221
#, kde-format
msgid "Force"
msgstr "बाधित करू"

#: optionsmodel.cpp:222
#, kde-format
msgid "The window property will be always forced to the given value."
msgstr ""

#: optionsmodel.cpp:224
#, kde-format
msgid "Force Temporarily"
msgstr "अस्थायी रूप सँ बाध्य करू"

#: optionsmodel.cpp:225
#, kde-format
msgid ""
"The window property will be forced to the given value until it is hidden\n"
"(this action will be deleted after the window is hidden)."
msgstr ""

#: rulesmodel.cpp:218
#, kde-format
msgid "Settings for %1"
msgstr "%1 लेल बिन्यास"

#: rulesmodel.cpp:221
#, fuzzy, kde-format
#| msgid "Window settings for %1"
msgid "New window settings"
msgstr "%1 क' लेल विंडो बिन्यास"

#: rulesmodel.cpp:237
#, kde-format
msgid ""
"You have specified the window class as unimportant.\n"
"This means the settings will possibly apply to windows from all "
"applications. If you really want to create a generic setting, it is "
"recommended you at least limit the window types to avoid special window "
"types."
msgstr ""

#: rulesmodel.cpp:244
#, kde-format
msgid ""
"Some applications set their own geometry after starting, overriding your "
"initial settings for size and position. To enforce these settings, also "
"force the property \"%1\" to \"Yes\"."
msgstr ""

#: rulesmodel.cpp:251
#, kde-format
msgid ""
"Readability may be impaired with extremely low opacity values. At 0%, the "
"window becomes invisible."
msgstr ""

#: rulesmodel.cpp:382
#, fuzzy, kde-format
#| msgid "De&scription:"
msgid "Description"
msgstr "वर्णन (&s):"

#: rulesmodel.cpp:382 rulesmodel.cpp:390 rulesmodel.cpp:398 rulesmodel.cpp:405
#: rulesmodel.cpp:411 rulesmodel.cpp:419 rulesmodel.cpp:424 rulesmodel.cpp:430
#, fuzzy, kde-format
#| msgid "&Window"
msgid "Window matching"
msgstr "विंडो (&W)"

#: rulesmodel.cpp:390
#, fuzzy, kde-format
#| msgid "Window &class (application type):"
msgid "Window class (application)"
msgstr "विंडो वर्ग (&c) (अनुप्रयोग प्रकार):"

#: rulesmodel.cpp:398
#, fuzzy, kde-format
#| msgid "Match w&hole window class"
msgid "Match whole window class"
msgstr "संपूर्ण विंडो वर्गक जोडी मिलाबू (&h)"

#: rulesmodel.cpp:405
#, fuzzy, kde-format
#| msgid "Match w&hole window class"
msgid "Whole window class"
msgstr "संपूर्ण विंडो वर्गक जोडी मिलाबू (&h)"

#: rulesmodel.cpp:411
#, fuzzy, kde-format
#| msgid "Window &types:"
msgid "Window types"
msgstr "विंडो प्रकार (&t):"

#: rulesmodel.cpp:419
#, fuzzy, kde-format
#| msgid "Window &role:"
msgid "Window role"
msgstr "विंडो भूमिका (&r):"

#: rulesmodel.cpp:424
#, fuzzy, kde-format
#| msgid "Window t&itle:"
msgid "Window title"
msgstr "विंडो शीर्षक (&i):"

#: rulesmodel.cpp:430
#, fuzzy, kde-format
#| msgid "&Machine (hostname):"
msgid "Machine (hostname)"
msgstr "मशीन (होस्ट-नाम): (&M)"

#: rulesmodel.cpp:436
#, fuzzy, kde-format
#| msgid "&Position"
msgid "Position"
msgstr "स्थिति (&P)"

#: rulesmodel.cpp:436 rulesmodel.cpp:442 rulesmodel.cpp:448 rulesmodel.cpp:453
#: rulesmodel.cpp:461 rulesmodel.cpp:467 rulesmodel.cpp:486 rulesmodel.cpp:502
#: rulesmodel.cpp:507 rulesmodel.cpp:512 rulesmodel.cpp:517 rulesmodel.cpp:522
#: rulesmodel.cpp:531 rulesmodel.cpp:546 rulesmodel.cpp:551 rulesmodel.cpp:556
#, fuzzy, kde-format
#| msgid "&Position"
msgid "Size & Position"
msgstr "स्थिति (&P)"

#: rulesmodel.cpp:442
#, fuzzy, kde-format
#| msgid "&Size"
msgid "Size"
msgstr "आकार (&S)"

#: rulesmodel.cpp:448
#, fuzzy, kde-format
#| msgid "Maximized &horizontally"
msgid "Maximized horizontally"
msgstr "आडा मे अधिकतम करू (&h)"

#: rulesmodel.cpp:453
#, fuzzy, kde-format
#| msgid "Maximized &vertically"
msgid "Maximized vertically"
msgstr "खडा मे अधिकतम करू (&v)"

#: rulesmodel.cpp:461
#, fuzzy, kde-format
#| msgid "All Desktops"
msgid "Virtual Desktop"
msgstr "सभ डेस्कटाप"

#: rulesmodel.cpp:467
#, fuzzy, kde-format
#| msgid "All Desktops"
msgid "Virtual Desktops"
msgstr "सभ डेस्कटाप"

#: rulesmodel.cpp:486
#, fuzzy, kde-format
#| msgid "A&ctive opacity in %"
msgid "Activities"
msgstr "एकरामे सक्रिय अपारदर्शिता (&c)_ %"

#: rulesmodel.cpp:502
#, fuzzy, kde-format
#| msgid "Splash Screen"
msgid "Screen"
msgstr "स्प्लैश स्क्रीन"

#: rulesmodel.cpp:507
#, fuzzy, kde-format
#| msgid "&Fullscreen"
msgid "Fullscreen"
msgstr "संपूर्ण स्क्रीन (&F)"

#: rulesmodel.cpp:512
#, fuzzy, kde-format
#| msgid "M&inimized"
msgid "Minimized"
msgstr "न्यूनतम (&i)"

#: rulesmodel.cpp:517
#, fuzzy, kde-format
#| msgid "Sh&aded"
msgid "Shaded"
msgstr "छाया (&a)"

#: rulesmodel.cpp:522
#, fuzzy, kde-format
#| msgid "P&lacement"
msgid "Initial placement"
msgstr "निर्धारित जगह (&l)"

#: rulesmodel.cpp:531
#, fuzzy, kde-format
#| msgid "Ignore requested &geometry"
msgid "Ignore requested geometry"
msgstr "निवेदित ज्यामितिक उपेक्षा करू (&g)"

#: rulesmodel.cpp:534
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some applications can set their own geometry, overriding the window manager "
"preferences. Setting this property overrides their placement requests.<nl/"
"><nl/>This affects <interface>Size</interface> and <interface>Position</"
"interface> but not <interface>Maximized</interface> or "
"<interface>Fullscreen</interface> states.<nl/><nl/>Note that the position "
"can also be used to map to a different <interface>Screen</interface>"
msgstr ""

#: rulesmodel.cpp:546
#, fuzzy, kde-format
#| msgid "M&inimum size"
msgid "Minimum Size"
msgstr "न्यूनतम आकार (&i)"

#: rulesmodel.cpp:551
#, fuzzy, kde-format
#| msgid "M&aximum size"
msgid "Maximum Size"
msgstr "अधिकतम आकार (&a)"

#: rulesmodel.cpp:556
#, kde-format
msgid "Obey geometry restrictions"
msgstr ""

#: rulesmodel.cpp:558
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some apps like video players or terminals can ask KWin to constrain them to "
"certain aspect ratios or only grow by values larger than the dimensions of "
"one character. Use this property to ignore such restrictions and allow those "
"windows to be resized to arbitrary sizes.<nl/><nl/>This can be helpful for "
"windows that can't quite fit the full screen area when maximized."
msgstr ""

#: rulesmodel.cpp:569
#, kde-format
msgid "Keep above other windows"
msgstr ""

#: rulesmodel.cpp:569 rulesmodel.cpp:574 rulesmodel.cpp:579 rulesmodel.cpp:585
#: rulesmodel.cpp:591 rulesmodel.cpp:597
#, kde-format
msgid "Arrangement & Access"
msgstr ""

#: rulesmodel.cpp:574
#, kde-format
msgid "Keep below other windows"
msgstr ""

#: rulesmodel.cpp:579
#, fuzzy, kde-format
#| msgid "Skip &taskbar"
msgid "Skip taskbar"
msgstr "काजपट्टी छोडू (&t)"

#: rulesmodel.cpp:581
#, kde-format
msgctxt "@info:tooltip"
msgid "Controls whether or not the window appears in the Task Manager."
msgstr ""

#: rulesmodel.cpp:585
#, fuzzy, kde-format
#| msgid "Skip pa&ger"
msgid "Skip pager"
msgstr "पेजर छोडू (&g)"

#: rulesmodel.cpp:587
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the Virtual Desktop manager."
msgstr ""

#: rulesmodel.cpp:591
#, fuzzy, kde-format
#| msgid "Skip pa&ger"
msgid "Skip switcher"
msgstr "पेजर छोडू (&g)"

#: rulesmodel.cpp:593
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the <shortcut>Alt+Tab</"
"shortcut> window list."
msgstr ""

#: rulesmodel.cpp:597
#, kde-format
msgid "Shortcut"
msgstr "शार्टकट"

#: rulesmodel.cpp:603
#, kde-format
msgid "No titlebar and frame"
msgstr ""

#: rulesmodel.cpp:603 rulesmodel.cpp:608 rulesmodel.cpp:614 rulesmodel.cpp:619
#: rulesmodel.cpp:625 rulesmodel.cpp:652 rulesmodel.cpp:680 rulesmodel.cpp:686
#: rulesmodel.cpp:698 rulesmodel.cpp:703 rulesmodel.cpp:709 rulesmodel.cpp:714
#, kde-format
msgid "Appearance & Fixes"
msgstr ""

#: rulesmodel.cpp:608
#, kde-format
msgid "Titlebar color scheme"
msgstr ""

#: rulesmodel.cpp:614
#, fuzzy, kde-format
#| msgid "A&ctive opacity in %"
msgid "Active opacity"
msgstr "एकरामे सक्रिय अपारदर्शिता (&c)_ %"

#: rulesmodel.cpp:619
#, fuzzy, kde-format
#| msgid "I&nactive opacity in %"
msgid "Inactive opacity"
msgstr "एकरामे अक्रिय अपारदर्शिता (&n)_ %"

#: rulesmodel.cpp:625
#, fuzzy, kde-format
#| msgid "&Focus stealing prevention"
msgid "Focus stealing prevention"
msgstr "फोकस स्टीलिंग प्रिवेंशन (&F)"

#: rulesmodel.cpp:627
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"KWin tries to prevent windows that were opened without direct user action "
"from raising themselves and taking focus while you're currently interacting "
"with another window. This property can be used to change the level of focus "
"stealing prevention applied to individual windows and apps.<nl/><nl/>Here's "
"what will happen to a window opened without your direct action at each level "
"of focus stealing prevention:<nl/><list><item><emphasis strong='true'>None:</"
"emphasis> The window will be raised and focused.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied, but "
"in the case of a situation KWin considers ambiguous, the window will be "
"raised and focused.</item><item><emphasis strong='true'>Normal:</emphasis> "
"Focus stealing prevention will be applied, but  in the case of a situation "
"KWin considers ambiguous, the window will <emphasis>not</emphasis> be raised "
"and focused.</item><item><emphasis strong='true'>High:</emphasis> The window "
"will only be raised and focused if it belongs to the same app as the "
"currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> The window will never be raised and focused.</item></list>"
msgstr ""

#: rulesmodel.cpp:652
#, fuzzy, kde-format
#| msgid "&Focus stealing prevention"
msgid "Focus protection"
msgstr "फोकस स्टीलिंग प्रिवेंशन (&F)"

#: rulesmodel.cpp:654
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"This property controls the focus protection level of the currently active "
"window. It is used to override the focus stealing prevention applied to new "
"windows that are opened without your direct action.<nl/><nl/>Here's what "
"happens to new windows that are opened without your direct action at each "
"level of focus protection while the window with this property applied to it "
"has focus:<nl/><list><item><emphasis strong='true'>None</emphasis>: Newly-"
"opened windows always raise themselves and take focus.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied to "
"the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will be raised and focused.</item><item><emphasis "
"strong='true'>Normal:</emphasis> Focus stealing prevention will be applied "
"to the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will <emphasis>not</emphasis> be raised and focused.</"
"item><item><emphasis strong='true'>High:</emphasis> Newly-opened windows "
"will only raise themselves and take focus if they belongs to the same app as "
"the currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> Newly-opened windows never raise themselves and take focus.</"
"item></list>"
msgstr ""

#: rulesmodel.cpp:680
#, fuzzy, kde-format
#| msgid "Accept &focus"
msgid "Accept focus"
msgstr "फोकस स्वीकारू (&f)"

#: rulesmodel.cpp:682
#, kde-format
msgid "Controls whether or not the window becomes focused when clicked."
msgstr ""

#: rulesmodel.cpp:686
#, fuzzy, kde-format
#| msgid "Block global shortcuts"
msgid "Ignore global shortcuts"
msgstr "वैश्विक शार्टकटसभ पर रोक लगाबू"

#: rulesmodel.cpp:688
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Use this property to prevent global keyboard shortcuts from working while "
"the window is focused. This can be useful for apps like emulators or virtual "
"machines that handle some of the same shortcuts themselves.<nl/><nl/>Note "
"that you won't be able to <shortcut>Alt+Tab</shortcut> out of the window or "
"use any other global shortcuts such as <shortcut>Alt+Space</shortcut> to "
"activate KRunner."
msgstr ""

#: rulesmodel.cpp:698
#, fuzzy, kde-format
#| msgid "&Closeable"
msgid "Closeable"
msgstr "बन्न करब योग्य (&C)"

#: rulesmodel.cpp:703
#, fuzzy, kde-format
#| msgid "Window &type"
msgid "Set window type"
msgstr "विंडो प्रकार (&t)"

#: rulesmodel.cpp:709
#, kde-format
msgid "Desktop file name"
msgstr ""

#: rulesmodel.cpp:714
#, kde-format
msgid "Block compositing"
msgstr ""

#: rulesmodel.cpp:766
#, fuzzy, kde-format
#| msgid "Window &class (application type):"
msgid "Window class not available"
msgstr "विंडो वर्ग (&c) (अनुप्रयोग प्रकार):"

#: rulesmodel.cpp:767
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application is not providing a class for the window, so KWin cannot use "
"it to match and apply any rules. If you still want to apply some rules to "
"it, try to match other properties like the window title instead.<nl/><nl/"
">Please consider reporting this bug to the application's developers."
msgstr ""

#: rulesmodel.cpp:801
#, fuzzy, kde-format
#| msgid "Window &types:"
msgid "All Window Types"
msgstr "विंडो प्रकार (&t):"

#: rulesmodel.cpp:802
#, kde-format
msgid "Normal Window"
msgstr "सामान्य विंडो"

#: rulesmodel.cpp:803
#, kde-format
msgid "Dialog Window"
msgstr "संवाद विंडो"

#: rulesmodel.cpp:804
#, kde-format
msgid "Utility Window"
msgstr "यूटिलिटी विंडो"

#: rulesmodel.cpp:805
#, kde-format
msgid "Dock (panel)"
msgstr "डाक करू (फलक)"

#: rulesmodel.cpp:806
#, kde-format
msgid "Toolbar"
msgstr "अओजारपट्टी"

#: rulesmodel.cpp:807
#, kde-format
msgid "Torn-Off Menu"
msgstr "टार्न-आफ मेनt"

#: rulesmodel.cpp:808
#, kde-format
msgid "Splash Screen"
msgstr "स्प्लैश स्क्रीन"

#: rulesmodel.cpp:809
#, kde-format
msgid "Desktop"
msgstr "डेस्कटाप"

#. i18n("Unmanaged Window")},  deprecated
#: rulesmodel.cpp:811
#, kde-format
msgid "Standalone Menubar"
msgstr "अलग-थलग मेनू-पट्टी"

#: rulesmodel.cpp:812
#, kde-format
msgid "On Screen Display"
msgstr ""

#: rulesmodel.cpp:822
#, kde-format
msgid "All Desktops"
msgstr "सभ डेस्कटाप"

#: rulesmodel.cpp:824
#, kde-format
msgctxt "@info:tooltip in the virtual desktop list"
msgid "Make the window available on all desktops"
msgstr ""

#: rulesmodel.cpp:843
#, kde-format
msgid "All Activities"
msgstr ""

#: rulesmodel.cpp:845
#, kde-format
msgctxt "@info:tooltip in the activity list"
msgid "Make the window available on all activities"
msgstr ""

#: rulesmodel.cpp:866
#, kde-format
msgid "Default"
msgstr "मूलभूत"

#: rulesmodel.cpp:867
#, kde-format
msgid "No Placement"
msgstr "कोनो स्थल नहि"

#: rulesmodel.cpp:868
#, kde-format
msgid "Minimal Overlapping"
msgstr ""

#: rulesmodel.cpp:869
#, fuzzy, kde-format
#| msgid "Maximizing"
msgid "Maximized"
msgstr "अधिकतम कएल जाए रहल अछि"

#: rulesmodel.cpp:870
#, kde-format
msgid "Centered"
msgstr "केंद्रित"

#: rulesmodel.cpp:871
#, kde-format
msgid "Random"
msgstr "क्रमहीन"

#: rulesmodel.cpp:872
#, fuzzy, kde-format
#| msgid "Top-Left Corner"
msgid "In Top-Left Corner"
msgstr "ऊप्पर बम्माँ कोना"

#: rulesmodel.cpp:873
#, kde-format
msgid "Under Mouse"
msgstr "माउस केर नीच्चाँ"

#: rulesmodel.cpp:874
#, kde-format
msgid "On Main Window"
msgstr "मुख्य विंडो पर"

#: rulesmodel.cpp:881
#, fuzzy, kde-format
#| msgctxt "no focus stealing prevention"
#| msgid "None"
msgid "None"
msgstr "किछु नहि"

#: rulesmodel.cpp:882
#, kde-format
msgid "Low"
msgstr "कम"

#: rulesmodel.cpp:883
#, kde-format
msgid "Normal"
msgstr "सामान्य"

#: rulesmodel.cpp:884
#, kde-format
msgid "High"
msgstr "बेसी"

#: rulesmodel.cpp:885
#, kde-format
msgid "Extreme"
msgstr "चरम"

#: rulesmodel.cpp:928
#, fuzzy, kde-format
#| msgid "On Main Window"
msgid "Unmanaged window"
msgstr "मुख्य विंडो पर"

#: rulesmodel.cpp:929
#, kde-format
msgid "Could not detect window properties. The window is not managed by KWin."
msgstr ""

#: ui/FileDialogLoader.qml:15
#, kde-format
msgid "Select File"
msgstr ""

#: ui/FileDialogLoader.qml:27
#, kde-format
msgid "KWin Rules (*.kwinrule)"
msgstr ""

#: ui/main.qml:62
#, kde-format
msgid "No rules for specific windows are currently set"
msgstr ""

#: ui/main.qml:63
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add New…</interface> button below to add some"
msgstr ""

#: ui/main.qml:71
#, kde-format
msgid "Select the rules to export"
msgstr ""

#: ui/main.qml:75
#, kde-format
msgid "Unselect All"
msgstr ""

#: ui/main.qml:75
#, kde-format
msgid "Select All"
msgstr ""

#: ui/main.qml:89
#, kde-format
msgid "Save Rules"
msgstr ""

#: ui/main.qml:100
#, fuzzy, kde-format
#| msgid "&New..."
msgid "Add New…"
msgstr "नया... (&N)"

#: ui/main.qml:111
#, kde-format
msgid "Import…"
msgstr ""

#: ui/main.qml:119
#, kde-format
msgid "Cancel Export"
msgstr ""

#: ui/main.qml:119
#, fuzzy, kde-format
#| msgid "Edit..."
msgid "Export…"
msgstr "संपादन..."

#: ui/main.qml:209
#, kde-format
msgid "Edit"
msgstr "संपादन"

#: ui/main.qml:218
#, kde-format
msgid "Duplicate"
msgstr ""

#: ui/main.qml:227
#, kde-format
msgid "Delete"
msgstr "मेटाबू"

#: ui/main.qml:240
#, kde-format
msgid "Import Rules"
msgstr ""

#: ui/main.qml:252
#, kde-format
msgid "Export Rules"
msgstr ""

#: ui/OptionsComboBox.qml:35
#, kde-format
msgid "None selected"
msgstr ""

#: ui/OptionsComboBox.qml:41
#, kde-format
msgid "All selected"
msgstr ""

#: ui/OptionsComboBox.qml:43
#, kde-format
msgid "%1 selected"
msgid_plural "%1 selected"
msgstr[0] ""
msgstr[1] ""

#: ui/RulesEditor.qml:63
#, kde-format
msgid "No window properties changed"
msgstr ""

#: ui/RulesEditor.qml:64
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Click the <interface>Add Property...</interface> button below to add some "
"window properties that will be affected by the rule"
msgstr ""

#: ui/RulesEditor.qml:85
#, fuzzy, kde-format
#| msgid "&Closeable"
msgid "Close"
msgstr "बन्न करब योग्य (&C)"

#: ui/RulesEditor.qml:85
#, fuzzy, kde-format
#| msgid "&New..."
msgid "Add Property..."
msgstr "नया... (&N)"

#: ui/RulesEditor.qml:98
#, kde-format
msgid "Detect Window Properties"
msgstr ""

#: ui/RulesEditor.qml:114 ui/RulesEditor.qml:121
#, kde-format
msgid "Instantly"
msgstr ""

#: ui/RulesEditor.qml:115 ui/RulesEditor.qml:126
#, kde-format
msgid "After %1 second"
msgid_plural "After %1 seconds"
msgstr[0] ""
msgstr[1] ""

#: ui/RulesEditor.qml:175
#, kde-format
msgid "Add property to the rule"
msgstr ""

#: ui/RulesEditor.qml:276 ui/ValueEditor.qml:54
#, kde-format
msgid "Yes"
msgstr ""

#: ui/RulesEditor.qml:276 ui/ValueEditor.qml:60
#, fuzzy, kde-format
#| msgctxt "no focus stealing prevention"
#| msgid "None"
msgid "No"
msgstr "किछु नहि"

#: ui/RulesEditor.qml:278 ui/ValueEditor.qml:168 ui/ValueEditor.qml:175
#, kde-format
msgid "%1 %"
msgstr ""

#: ui/RulesEditor.qml:280
#, kde-format
msgctxt "Coordinates (x, y)"
msgid "(%1, %2)"
msgstr ""

#: ui/RulesEditor.qml:282
#, kde-format
msgctxt "Size (width, height)"
msgid "(%1, %2)"
msgstr ""

#: ui/ValueEditor.qml:203
#, kde-format
msgctxt "(x, y) coordinates separator in size/position"
msgid "x"
msgstr ""

#, fuzzy
#~| msgid "WId of the window for special window settings."
#~ msgid "KWin id of the window for special window settings."
#~ msgstr "विशिष्ट विंडो सेटिंग क' लेल विंडो क' डबल्यूआईडी"

#~ msgid "Whether the settings should affect all windows of the application."
#~ msgstr "सेटिंग की अनुप्रयोग केर सबहि विंडो पर प्रभाव डालैछ"

#~ msgid "This helper utility is not supposed to be called directly."
#~ msgstr "ई मद्दति यूटिलिटी सीधे बुलाएल नहि जाए सकैत अछि."

#, fuzzy
#~| msgid "Edit Window-Specific Settings"
#~ msgctxt "Window caption for the application wide rules dialog"
#~ msgid "Edit Application-Specific Settings"
#~ msgstr "विंडो-विशिष्ट बिन्यास संपादित करू"

#~ msgid "Edit Window-Specific Settings"
#~ msgstr "विंडो-विशिष्ट बिन्यास संपादित करू"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "संगीता कुमारी"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sangeeta09@gmail.com"

#, fuzzy
#~| msgid "Window &role:"
#~ msgid "Window Rules"
#~ msgstr "विंडो भूमिका (&r):"

#, fuzzy
#~| msgid "Cascade"
#~ msgid "Cascaded"
#~ msgstr "प्रपाती"

#, fuzzy
#~| msgid "Keep &above"
#~ msgid "Keep above"
#~ msgstr "उप्पर राखू (&A)"

#, fuzzy
#~| msgid "Keep &below"
#~ msgid "Keep below"
#~ msgstr "नीच्चाँ राखू (&B)"

#~ msgid "KWin"
#~ msgstr "के-विन"

#~ msgid "KWin helper utility"
#~ msgstr "के-विन मद्दति यूटिलिटी"

#~ msgid "Override Type"
#~ msgstr "ओवरराइड टाइप"

#~ msgid "Unknown - will be treated as Normal Window"
#~ msgstr "अज्ञात - सामान्य विंडो केर रूपेँ मानल जएताह"

#~ msgid "Information About Selected Window"
#~ msgstr "चयनित विंडो केर संबंध मे जानकारी"

#~ msgid "Class:"
#~ msgstr "वर्गः"

#~ msgid "Role:"
#~ msgstr "भूमिका:"

#~ msgid "Type:"
#~ msgstr "प्रकार:"

#~ msgid "Title:"
#~ msgstr "शीर्षक:"

#~ msgid "Machine:"
#~ msgstr "मशीनः"

#~ msgid "&Single Shortcut"
#~ msgstr "एकल शार्टकट... (&S)"

#~ msgid "C&lear"
#~ msgstr "साफ करू (&l)"

#~ msgid "Window-Specific Settings Configuration Module"
#~ msgstr "विंडो-विशिष्ट बिन्यास कान्फिगरेशन मोड्यूल"

#~ msgid "(c) 2004 KWin and KControl Authors"
#~ msgstr "(c) 2004 के-विन आओर के-कन्ट्रोल लेखक"

#~ msgid "Lubos Lunak"
#~ msgstr "लुबास लुनाक"

#~ msgid "Remember settings separately for every window"
#~ msgstr "प्रत्येक विंडो क' लेल बिन्याससभ केँ अलग-अलग याद राखू"

#~ msgid "Show internal settings for remembering"
#~ msgstr "याद रखबाक लेल आंतरिक बिन्याससभ केँ देखाबू"

#~ msgid "Internal setting for remembering"
#~ msgstr "याद रखबा क' लेल आंतरिक बिन्यास"

#~ msgid "&Modify..."
#~ msgstr "सुधारू... (&M)"

#~ msgid "Move &Up"
#~ msgstr "उप्पर जाउ (&U)"

#~ msgid "Move &Down"
#~ msgstr "नीच्चाँ जाउ (&D)"

#~ msgid ""
#~ "Enable this checkbox to alter this window property for the specified "
#~ "window(s)."
#~ msgstr "उल्लेखित विंडो केर विंडो-गुणसभ केँ बदलब क' लेल एहि चेक-बक्सा केँ सक्षम करू."

#~ msgid "Unnamed entry"
#~ msgstr "अनाम प्रविष्टि"

#~ msgid "Consult the documentation for more details."
#~ msgstr "बाइली विवरणसभ क' लेल दस्ताबेज केँ देखू"

#~ msgid "Edit Shortcut"
#~ msgstr "शार्टकट संपादित करू"

#~ msgid "0123456789-+,xX:"
#~ msgstr "0123456789-+,xX:"

#~ msgid "&Desktop"
#~ msgstr "डेस्कटाप (&D)"

#~ msgid "Smart"
#~ msgstr "स्मार्ट"

#~ msgid "kcmkwinrules"
#~ msgstr "केसीएम-केविन-रूल्स"

#~ msgid "Opaque"
#~ msgstr "अपारदर्शक"

#~ msgid "Transparent"
#~ msgstr "पारदर्शी"

#~ msgid "&Moving/resizing"
#~ msgstr "घसकाएनाइ / नवीन आकार देनाइ (&M)"

#, fuzzy
#~| msgid "Title:"
#~ msgid "Tiled"
#~ msgstr "शीर्षक:"

#~ msgid "Use window &class (whole application)"
#~ msgstr "विंडो वर्ग उपयोग करू (&c) (सम्पूर्ण अनुप्रयोग)"

#~ msgid "Use window class and window &role (specific window)"
#~ msgstr "विंडो वर्ग आओर विंडो भूमिका उपयोग करू (&r) (विशिष्ट विंडो)"

#~ msgid "Use &whole window class (specific window)"
#~ msgstr "सम्पूर्ण विंडो वर्ग उपयोग करू (विशिष्ट विंडो)"

#~ msgid "Match also window &title"
#~ msgstr "विंडो शीर्षको केँ मिलान करू (&t)"

#~ msgid "Extra role:"
#~ msgstr "बाइली भूमिका:"

#~ msgid "Window &Extra"
#~ msgstr "बाइली विंडो (&E)"

#~ msgid "&Geometry"
#~ msgstr "ज्यामिति (&G)"

#~ msgid "&Preferences"
#~ msgstr "वरीयतासभ (&P)"

#~ msgid "&No border"
#~ msgstr "कोनो किनारा नहि (&N)"

#~ msgid "0123456789"
#~ msgstr "0123456789"

#~ msgid "W&orkarounds"
#~ msgstr "कार्य-विकल्प (&o)"

#~ msgid "Strictly obey geometry"
#~ msgstr "ज्यामिति क' सख्ती सँ पालन करू"
