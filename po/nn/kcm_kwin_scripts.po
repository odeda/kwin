# Translation of kcm_kwin_scripts to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2015, 2017, 2018, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-21 02:02+0000\n"
"PO-Revision-Date: 2022-09-22 21:56+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.1\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: module.cpp:50
#, kde-format
msgid "Import KWin Script"
msgstr "Importer KWin-skript"

#: module.cpp:51
#, kde-format
msgid "*.kwinscript|KWin scripts (*.kwinscript)"
msgstr "*.kwinscript|KWin-skript (*.kwinscript)"

#: module.cpp:62
#, kde-format
msgctxt "Placeholder is error message returned from the install service"
msgid ""
"Cannot import selected script.\n"
"%1"
msgstr ""
"Klarte ikkje importera det valde skriptet.\n"
"%1"

#: module.cpp:66
#, kde-format
msgctxt "Placeholder is name of the script that was imported"
msgid "The script \"%1\" was successfully imported."
msgstr "Skriptet «%1» er no importert."

#: module.cpp:125
#, kde-format
msgid "Error when uninstalling KWin Script: %1"
msgstr "Feil ved avinstallering av KWin-skript: %1"

#: ui/main.qml:23
#, fuzzy, kde-format
#| msgid "Install from File..."
msgid "Install from File…"
msgstr "Installer frå fil …"

#: ui/main.qml:27
#, fuzzy, kde-format
#| msgid "Get New Scripts..."
msgctxt "@action:button get new KWin scripts"
msgid "Get New…"
msgstr "Hent nye skript …"

#: ui/main.qml:65
#, fuzzy, kde-format
#| msgctxt "@info:tooltip"
#| msgid "Delete..."
msgctxt "@info:tooltip"
msgid "Delete…"
msgstr "Slett …"
